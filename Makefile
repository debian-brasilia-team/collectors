include .env
export


build:
	podman build -t $(REGISTRY):$(VERSION) .

run-udd:
	podman run --env-file .env -v `pwd`:/app --rm $(REGISTRY):$(VERSION) udd.py

run-salsa:
	podman run --env-file .env -v `pwd`:/app --rm $(REGISTRY):$(VERSION) salsa.py

lint:
	podman run -it --rm \
		-v `pwd`:/app \
		$(REGISTRY):$(VERSION) \
		-m flake8

shell:
	podman run -it --rm \
		-v `pwd`:/app \
		--network host \
		--entrypoint sh \
		$(REGISTRY):$(VERSION)

freeze:
	podman run -it --rm \
		-v `pwd`:/app \
		--network host \
		--entrypoint pip \
		$(REGISTRY):$(VERSION) \
		freeze > requirements.txt

