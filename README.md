# collectors

Scripts that collect information from Debian Brasília [Salsa](https://salsa.debian.org) issues and [Ultimate Debian Database](https://wiki.debian.org/UltimateDebianDatabase/) (UDD) storing it inside a  private minio datalake.

# Dependencies

- Podman
- Python >= 3
- Make

```bash
apt install python3-venv python3-pip python3-dev podman
```

## Development

| Name       | Command          | Description                                                       |
|------------|------------------|-------------------------------------------------------------------|
| Build      | `make build`     | Build the Dockerfile into a Docker image.                         |
| Run udd    | `make run-udd`   | Execute podman and run udd script.                                |
| Run salsa  | `make run-salsa` | Execute podman and run salsa script.                              |
| Lint       | `make lint`      | Check lint flake8 lint policies defined at `.flake8`.             |
| Shell      | `make shell`     | Init a container but with shell attached for debug.               |
| Freeze     | `make freeze`    | Lock pip dependencies to `requirements.txt` file.                 |


## Deploy

Add the following paraments to the pipeline secrets.

- `GITLAB_TOKEN`
- `MINIO_ACCESS_KEY`
- `MINIO_SECRET_KEY`


Then run the pipeline and follow the job logs.


## Logging

The logger methods are named after the level or severity of the events they are used to track. The standard levels and their applicability are described below (in increasing order of severity):

| Level      | When it’s used                                                                                                                                                         |
|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `DEBUG`    | Detailed information, typically of interest only when diagnosing problems.                                                                                             |
| `INFO`     | Confirmation that things are working as expected.                                                                                                                      |
| `WARNING`  | An indication that something unexpected happened, or indicative of some problem in the near future (e.g. ‘disk space low’). The software is still working as expected. |
| `ERROR`    | Due to a more serious problem, the software has not been able to perform some function.                                                                                |
| `CRITICAL` | A serious error, indicating that the program itself may be unable to continue running.                                                                                 |

## Setup and Run manually

```bash
# Create environment
python3 -m venv venv

# To activate the environment
source venv/bin/activate

python3 -m pip install -r requirements.txt

# Run udd script
python3 udd.py

# Run salsa script
python3 salsa.py

# When you finish you can exit typing
deactivate

# Lock pip dependencies to requirements.txt file
python3 -m pip freeze > requirements.txt
```

## License

This app is licensed under the Apache 2 License. For more details, please see [LICENSE](https://salsa.debian.org/debian-brasilia-team/collectors/blob/main/LICENSE).
