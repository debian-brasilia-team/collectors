argon2-cffi==23.1.0
argon2-cffi-bindings==21.2.0
certifi==2024.2.2
cffi==1.16.0
charset-normalizer==3.3.2
flake8==7.0.0
idna==3.6
mccabe==0.7.0
minio==7.2.5
psycopg2-binary==2.9.9
pycodestyle==2.11.1
pycparser==2.21
pycryptodome==3.20.0
pyflakes==3.2.0
python-gitlab==4.4.0
requests==2.31.0
requests-toolbelt==1.0.0
typing_extensions==4.10.0
Unidecode==1.3.8
urllib3==2.2.1
