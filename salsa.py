import gitlab

from os import getenv
from datetime import datetime
from minio.error import S3Error
from src.minio import minio_client, upload_minio
from src.logger import logger


# Get GitLab credentials from environment variables
# Make sure you have this environment variable set
gitlab_token = getenv('GITLAB_TOKEN')

# Create a GitLab client instance
gitlab_url = 'https://salsa.debian.org'
gitlab_client = gitlab.Gitlab(gitlab_url, private_token=gitlab_token)

# Project ID or Path (namespace/project_name) of the Salsa repository
project_id_or_path = 'debian-brasilia-team/docs'
BUCKET_NAME = "salsa"

try:
    if not minio_client.bucket_exists(BUCKET_NAME):
        minio_client.make_bucket(BUCKET_NAME)
except S3Error as e:
    logger.error(f"Error: {e}")


def get_path(project_id, issue_iid):
    now = datetime.now()
    year = now.year
    month = now.month
    day = now.day

    path = f"{year}/{month}/{day}/{project_id}/{issue_iid}.json.gz"
    logger.info(f'Object path: {path}')
    return path


def fetch_issues():
    try:
        # Get the project by ID or Path
        project = gitlab_client.projects.get(project_id_or_path)

        # Fetch issues from the project
        issues = project.issues.list(
            order_by='created_at',
            sort='desc',
            all=True
        )

    except gitlab.exceptions.GitlabAuthenticationError:
        logger.error(
            "Failed to authenticate with GitLab."
        )
    except gitlab.exceptions.GitlabGetError:
        logger.error(
            f"Failed to get project with ID or Path '{project_id_or_path}'."
        )
    except Exception as e:
        logger.error(f"An error occurred: {e}")

    return issues


if __name__ == '__main__':

    issues = fetch_issues()

    for issue in issues:
        project_id = issue.project_id
        issue_iid = issue.iid
        object_path = get_path(project_id, issue_iid)

        comments = issue.notes.list(get_all=True)

        issue_obj = issue.asdict()
        issue_obj["comments"] = []

        for comment in comments:
            issue_obj["comments"].append(comment.asdict())

        upload_minio(BUCKET_NAME, issue_obj, object_path)
