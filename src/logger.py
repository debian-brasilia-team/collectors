import logging
from os import getenv


logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=getenv("LOGLEVEL", logging.INFO)
)

logger = logging.getLogger(__name__)
