from minio.error import S3Error
from minio import Minio
from os import getenv
from .logger import logger
from .utils import str_to_bool
import json
import gzip
import io

minio_client = Minio(
    getenv("MINIO_HOST", "bucket-api.debianbsb.org"),
    access_key=getenv("MINIO_ACCESS_KEY") or getenv("MINIO_ROOT_USER"),
    secret_key=getenv("MINIO_SECRET_KEY") or getenv("MINIO_ROOT_PASSWORD"),
    # Set secure to False for HTTP connection
    secure=str_to_bool(getenv("MINIO_SECURE", "true"))
)


def upload_minio(bucket_name, payload, object_path):
    data = json.dumps(payload).encode('utf-8')
    compressed_data = gzip.compress(data)

    try:
        minio_client.put_object(
            bucket_name,
            object_path,
            io.BytesIO(compressed_data),
            len(compressed_data)
        )

        logger.info(f"Successfully uploaded to '{bucket_name}' bucket")
    except S3Error as e:
        logger.error(f"Error: {e}")
