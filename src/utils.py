def str_to_bool(s):
    return s.lower() in ['true', 'yes', 'on', '1']
