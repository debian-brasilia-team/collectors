import psycopg2

from unidecode import unidecode
from datetime import datetime
from minio.error import S3Error
from src.minio import minio_client, upload_minio
from src.logger import logger


contributors = [
    'thaysreb@gmail.com',  # Thais Reboucas Araujo
    'gpivetta99@gmail.com',  # Gabriela Pivetta
    'jlima.oliveira11@gmail.com',  # Joao Paulo Lima de Oliveira
    'francisco170142329@gmail.com',  # Francisco Emanoel
    'sergiosacj@protonmail.com',  # Sergio de Almeida Cipriano Junior
    'sergiosacj@hotmail.com.br',  # Sergio de Almeida Cipriano Junior
    'sergiosacj@riseup.net',  # Sergio de Almeida Cipriano Junior
    'rbrandao@protonmail.com',  # Ricardo Brandao
    'vitorfleonardo@gmail.com',  # Vitor Feijo Leonardo
    'aquilamacedo@riseup.net',  # Aquila Macedo Costa
    'arthurbdiniz@gmail.com',  # Arthur Diniz
    'guilherme@puida.xyz',  # Guilherme Puida
    'charlesmelara@outlook.com',  # Carlos Henrique Lima Melara
    'charlesmelara@riseup.net',  # Carlos Henrique Lima Melara
    'leandrocunha016@gmail.com',  # Leandro Cunha
    'mvdeus574@gmail.com',  # Marcos Deus
    'lucasgabriel_df@hotmail.com',  # Lucas Gabriel Sousa Camargo Paiva
    'raul.cheleguini@gmail.com'  # Raul Cheleguini
]

BUCKET_NAME = "udd"


try:
    if not minio_client.bucket_exists(BUCKET_NAME):
        minio_client.make_bucket(BUCKET_NAME)
except S3Error as e:
    logger.error(f"Error: {e}")


def get_path(contributor_email):
    now = datetime.now()
    year = now.year
    month = now.month
    day = now.day

    path = f"{year}/{month}/{day}/{contributor_email}.json.gz"
    logger.info(f'Object path: {path}')
    return path


def fetch_udd(contributor):
    try:
        connection = psycopg2.connect(
            user="udd-mirror",
            password="udd-mirror",
            host="udd-mirror.debian.net",
            port="5432",
            database="udd",
            client_encoding='UTF8'
        )

        cursor = connection.cursor()

        contributor = unidecode(contributor)
        contributor = contributor.replace(" ", "%")
        logger.info(contributor)
        sql_select_query = f"SELECT * FROM public.upload_history WHERE changed_by_email LIKE '%{contributor}%' ORDER BY date;"  # noqa: E501

        cursor.execute(sql_select_query)

        logger.info(
            f'Selecting {contributor} rows from table using cursor'
        )
        records = cursor.fetchall()

        column_names = [description[0] for description in cursor.description]

        contributor_data = []
        for row in records:
            contribution_data = {}

            for idx, item in enumerate(row):
                if type(item) is datetime:
                    contribution_data[column_names[idx]] = item.isoformat()
                    continue

                contribution_data[column_names[idx]] = item

            contributor_data.append(contribution_data)

    except (Exception, psycopg2.Error) as error:
        logger.error("Error while fetching data from PostgreSQL", error)

    finally:
        # closing database connection.
        if connection:
            cursor.close()
            connection.close()
            logger.info("PostgreSQL connection is closed")

    return contributor_data


if __name__ == '__main__':
    for contributor in contributors:
        payload = fetch_udd(contributor)
        object_path = get_path(contributor)
        upload_minio(BUCKET_NAME, payload, object_path)
